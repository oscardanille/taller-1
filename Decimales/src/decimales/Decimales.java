/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package decimales;
import java.util.Scanner;
/**
 *
 * @author DANIELEC
 */
public class Decimales {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double[] numeros = new double[6];

        // Pedir al usuario que ingrese 6 números entre 15.12 y 19.31
        for (int i = 0; i < 6; i++) {
            while (true) {
                System.out.print("Ingrese el valor " + (i+1) + ": ");
                double num = scanner.nextDouble();
                if (num >= 15.12 && num <= 19.31) {
                    numeros[i] = num;
                    break;
                } else {
                    System.out.println("El valor ingresado no está dentro del rango permitido.");
                }
            }
        }

        // Calcular el resultado y mostrarlo al usuario
        for (double num : numeros) {
            double resultado = ((num * 100) / 2) + 100;
            System.out.println("El resultado de ((" + num + "*100)/2)+100 es " + resultado);
        }
    }
}
