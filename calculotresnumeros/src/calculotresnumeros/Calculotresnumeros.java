/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package calculotresnumeros;
import java.util.Scanner; 
/**
 *
 * @author DANIELEC
 */
public class Calculotresnumeros {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese la cantidad de veces que desea realizar los calculos: ");
        int cantidad = input.nextInt();

        for (int i = 0; i < cantidad; i++) {
            System.out.println("Ingrese los tres numeros separados por espacios:");
            int num1 = input.nextInt();
            int num2 = input.nextInt();
            int num3 = input.nextInt();

            System.out.println("Ingrese la operacion que desea realizar (+, -, *, /, %):");
            char operacion = input.next().charAt(0);

            double resultado;
            switch (operacion) {
                case '+':
                    resultado = num1 + num2 + num3;
                    System.out.println("El resultado de la suma es: " + resultado);
                    break;
                case '-':
                    resultado = num1 - num2 - num3;
                    System.out.println("El resultado de la resta es: " + resultado);
                    break;
                case '*':
                    resultado = num1 * num2 * num3;
                    System.out.println("El resultado de la multiplicacion es: " + resultado);
                    break;
                case '/':
                    if (num2 == 0 || num3 == 0) {
                        System.out.println("No se puede dividir por cero.");
                    } else {
                        resultado = (double) num1 / num2 / num3;
                        System.out.println("El resultado de la division es: " + resultado);
                    }
                    break;
                case '%':
                    if (num2 == 0 || num3 == 0) {
                        System.out.println("No se puede calcular el modulo por cero.");
                    } else {
                        resultado = num1 % num2 % num3;
                        System.out.println("El resultado del modulo es: " + resultado);
                    }
                    break;
                default:
                    System.out.println("Operacion no valida.");
            }
        }

        input.close();
    }
}