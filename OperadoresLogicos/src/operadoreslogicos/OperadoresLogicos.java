/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package operadoreslogicos;
import java.util.Scanner;
/**
 *
 * @author DANIELEC
 */
public class OperadoresLogicos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         Scanner sc = new Scanner(System.in);
        
        // Pedir la cantidad de veces que se realizará la operación booleana
        System.out.print("Ingrese la cantidad de veces que desea realizar la operacion booleana: ");
        int n = sc.nextInt();
        
        // Pedir el tipo de operación booleana que se desea realizar
        System.out.print("Ingrese el tipo de operacion booleana que desea realizar (AND, OR o XOR): ");
        String operacion = sc.next();
        
        // Validar que la operación ingresada sea válida
        if (!operacion.equals("AND") && !operacion.equals("OR") && !operacion.equals("XOR")) {
            System.out.println("Operación no válida. Debe ingresar AND, OR o XOR.");
        } else {
            // Pedir los valores booleanos para realizar la operación
            for (int i = 0; i < n; i++) {
                System.out.print("Ingrese el primer valor booleano: ");
                boolean a = sc.nextBoolean();
                
                System.out.print("Ingrese el segundo valor booleano: ");
                boolean b = sc.nextBoolean();
                
                // Realizar la operación booleana
                boolean resultado;
                if (operacion.equals("AND")) {
                    resultado = a && b;
                } else if (operacion.equals("OR")) {
                    resultado = a || b;
                } else {
                    resultado = a ^ b;
                }
                
                // Imprimir el resultado
                System.out.printf("El resultado de la operación %s entre %s y %s es %s\n", operacion, a, b, resultado);
            }
        }
        
        sc.close();
    }
}
    
