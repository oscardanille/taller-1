/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package facto123;

import java.util.Scanner;

/**
 *
 * @author DANIELEC
 */
public class Facto123 {
public static int factorial (int n){
if (n==0){
return 1;
} else {
return n*factorial (n-1);
}
}
    public static void main(String[] args) {
    Scanner fact = new Scanner(System.in); 
    try {
   for (int i = 1; i <= 10; i++){
       System.out.println("Ingrese un numero entero entre 1 y 10 para calcular su factorial");
       int num = fact.nextInt();
       System.out.println("El factorial de " + num + " es: " + factorial (num));
   } 
    }
   catch (Exception e){
       System.out.println("Error: " + e);
   }
    }
    
}

