/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package validacionnumeros;
import java.util.Scanner;
import java.util.ArrayList;
/**
 *
 * @author DANIELEC
 */
public class ValidacionNumeros {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Integer> numeros = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            System.out.print("Introduce un número par entre 74 y 189: ");
            int num = sc.nextInt();
            if (validarNumero(num)) {
                System.out.println("Número validado");
                numeros.add(num);
            } else {
                System.out.println("Número no válido");
                i--; // Se resta 1 al índice para pedir el número de nuevo.
            }
        }

        System.out.println("Los números introducidos son: " + numeros);
    }

    public static boolean validarNumero(int num) {
        return num >= 74 && num <= 189 && num % 2 == 0 && num % 5 == 0;
    }
}